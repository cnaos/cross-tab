#!/usr/bin/env ruby
# coding: utf-8
#
# Copyright (c) 2019 cnaos. All rights reserved.


class CrossTab
    attr_accessor :title
    attr_reader :header_label_array
    attr_reader :row_map

    def initialize()
        @header_label_array = Array.new
        @row_map = Hash.new { |h,k| h[k] = {} }
    end

    def print_as_tsv(output)
        output.puts "#{@title}\t#{@header_label_array.join("\t")}"
        @row_map.sort.each{|key,value|
            data = Array.new
            @header_label_array.each{|label|
                data.push(value[label] || 0)
            }
            output.puts "#{key}\t#{data.join("\t")}"
        }
    end
end


cross_tab = CrossTab.new

# ヘッダ行の処理
header_line = ARGF.gets
header_line.chomp!
(key_label, group, appendix_array_data) = header_line.split("\t",3)

cross_tab.title = key_label
appendix_array = appendix_array_data.split("\t")

#データ部分の処理
while line = ARGF.gets
    line.chomp!
    (key, group, data_array_text) = line.split("\t",3)
    data_array = data_array_text.split("\t")
    column_map = cross_tab.row_map[key]

    appendix_array.each_with_index{|appendix,index|
        data_label = "#{group}_#{appendix}"
        unless cross_tab.header_label_array.include? data_label
            cross_tab.header_label_array.push(data_label)
        end
        column_map[data_label] = data_array[index]
    }
end

# 出力する
cross_tab.print_as_tsv(STDOUT)
